/*var express = require("express");
var app = express();
app.use(express.json());
app.listen(3000, () => console.log("Server running on port 3000"));

app.get("/url", (req, res, next) => res.json(["Paris","Barcelona","Valencia","Madrid"]));

var misDestinos = [];
app.get("/my", (req, res, next) => res.json(misDestinos));
app.post("/my", (req, res, next) => {
    console.log(req.body);
    misDestinos = req.body;
    res.json(misDestinos);
});*/


var express = require("express"), cors = require("cors");
var app = express();
app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log("Server running on port 3000"));

app.get("/url", (req, res, next) => res.json(["Paris","Barcelona","Valencia","Madrid"]));

var ciudades = ["Barcelona", "Paris", "Barranquilla", "Montevideo", "Nueva York", "Mexico DF"];
app.get(
    "/ciudades",
    (req, res, next) => res.json(
        ciudades.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1)
    ));

var misDestinos = [];
app.get("/my", (req, res, next) => res.json(misDestinos));
app.post("/my", (req, res, next) => {
    console.log(req.body);
    //misDestinos = req.body;
    misDestinos.push(req.body.nuevo);
    res.json(misDestinos);
});

app.get(
    "/api/translation",
    (req, res, next) => res.json([
        {lang: req.query.lan, key: 'HOLA', value: 'HOLA ' + req.query.lang}
    ]));
