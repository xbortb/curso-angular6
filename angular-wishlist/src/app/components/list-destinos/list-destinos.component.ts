import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../../models/destinos-viajes-state.model';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinosApiClient } from './../../models/destinos-api-client.model';

@Component({
  selector: 'app-list-destinos',
  templateUrl: './list-destinos.component.html',
  styleUrls: ['./list-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(
    public destinosApiClient:DestinosApiClient, 
    private store: Store<AppState>
    ) { 
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    //Solo nos importan las actualizaciones de destinos.favorito a Store (es decir cuando cambia el destino Favorito)
    this.store.select(state => state.destinos.favorito)
      .subscribe(d =>  {
        // Sobre 'd' llega lo que ha cambiado
        if (d != null) {
          this.updates.push("Se ha elegido a '" + d.nombre + "'");
      }
      });

    
    //all de forma reactiva, se actualiza con los cambios en state.destinos.items
    store.select(state => state.destinos.items).subscribe(items => this.all = items);

    //Nos subscribimos al observable. Llega un callback con el objeto de tipo DestinoViaje que ha pasado a ser el current
    /*this.destinosApiClient.subscribeOnChange((d: DestinoViaje) => {
      // Diferente de null para descartar la inicializacio "current: Subject<DestinoViaje> = new BehaviorSubject<DestinoViaje>(null);"
      // que tambien llega aqui como cambio de valor de current.
      if (d != null) {
        this.updates.push("Se ha elegido a '" + d.nombre + "'");
    }
    });*/
  }

  ngOnInit(): void {
  }

	agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    // Disparar un action cuando se añade un nuevo destino
    // Se elimina porque ya lo hace la ApiClient
    //this.store.dispatch(new NuevoDestinoAction(d))
  }
  
  elegido(e: DestinoViaje) {
    this.destinosApiClient.elegir(e);
    // Disparar un action cuando se elije un nuevo destino como favorito
    // Se elimina porque ya lo hace la ApiClient
    //this.store.dispatch(new ElegidoFavoritoAction(e))
  }

  getAll() {

  }
}
