import { Component, EventEmitter, OnInit, Output, Inject, forwardRef } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax } from 'rxjs/ajax';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];

  //FormBuilder nos permite construir el FormGroup (formulario)
  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG )) private config: AppConfig) { 
    this.onItemAdded = new EventEmitter();
    //al group() le pasamos un objeto que nos define la estructura de nuestro formulario (que controles vamos a poder vincular)
    this.fg = fb.group({
      nombre: ['', Validators.compose([
        Validators.required,          //el nombre no puede estar vacio => requerido
        this.nombreValidator,          //validador personalizado
        this.nombreValidatorParametrizable(this.minLongitud) //validador personalizado
      ])], 
      url: ['']
    });

    //Registrar un observador sobre todo canvio que se haga sobre el formulario
    this.fg.valueChanges.subscribe((form: any) => {
      console.log('cambio en el formulario:', form);
    });
  }

  ngOnInit(): void {
    const elemNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemNombre, 'input')
      .pipe(
        map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
        filter(text => text.length >= 2),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
        ).subscribe(ajaxResponse => this.searchResults = ajaxResponse.response);
/*      // Version sin crida API rest
        switchMap(() => ajax('/assets/datos.json'))
      ).subscribe(ajaxResponse => {
          console.log(ajaxResponse)
          this.searchResults = ajaxResponse.response;
      });*/
  }

  guardar(nombre: string, url:string): boolean {
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidator(control: FormControl): { [s: string]: boolean } {
    //longitud del input text introducido
    const l = control.value.toString().trim().length;
    // solo valida quan la longitud es mayor que 0 y menor que 5
    if(l > 0 && l < 5){
      //objeto devuelto al estilo JSON (key:value)
      return { invalidNombre: true};
    }
    return null;
  }

  nombreValidatorParametrizable(minLong: number): ValidatorFn {
    return (control: FormControl): { [s: string]: boolean } | null => {
      const l = control.value.toString().trim().length;
      if(l > 0 && l < minLong){
        //objeto devuelto al estilo JSON (key:value)
        return { minLongNombre: true};
      }
      return null;
    }
  }

}
