import {
    reducerDestinosViajes,
    DestinosViajesState,
    initializeDestinosViajesState,
    InitMyDataAction,
    NuevoDestinoAction,
    ElegidoFavoritoAction,
    VoteUpAction,
    VoteDownAction,
    VoteResetAction
} from './destinos-viajes-state.model';

import { DestinoViaje } from './destino-viaje.model';

describe('reducerDestinosViaje', () => {
    it('should reduce init data', () => {
        // setup
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
        // action
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        // asserts
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual('destino 1');
    });

    it('should reduce new item added', () => {
        // setup
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('barcelona','url'));
        // action
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        // asserts
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual('barcelona');
    });

    it('should reduce elegido favorito', () => {
        // setup
        var destino = new DestinoViaje('barcelona','url');
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action1: NuevoDestinoAction = new NuevoDestinoAction(destino);
        // actions
        const prevState2 = reducerDestinosViajes(prevState, action1);
        const action2: ElegidoFavoritoAction = new ElegidoFavoritoAction(destino);
        const newState: DestinosViajesState = reducerDestinosViajes(prevState2, action2);
        // asserts
        expect(newState.favorito.nombre).toEqual('barcelona');
    });

    it('should reduce vote up', () => {
        // setup
        var destino = new DestinoViaje('barcelona','url');
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action1: NuevoDestinoAction = new NuevoDestinoAction(destino);
        // actions
        const prevState2 = reducerDestinosViajes(prevState, action1);
        const action2: VoteUpAction = new VoteUpAction(destino);
        const newState: DestinosViajesState = reducerDestinosViajes(prevState2, action2);
        // asserts
        expect(newState.items[0].votes).toEqual(1);
    });

    it('should reduce vote down', () => {
        // setup
        var destino = new DestinoViaje('barcelona','url');
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action1: NuevoDestinoAction = new NuevoDestinoAction(destino);
        // actions
        const prevState2 = reducerDestinosViajes(prevState, action1);
        const action2: VoteDownAction = new VoteDownAction(destino);
        const newState: DestinosViajesState = reducerDestinosViajes(prevState2, action2);
        // asserts
        expect(newState.items[0].votes).toEqual(-1);
    });

    it('should reduce vote reset', () => {
        // setup
        var destino = new DestinoViaje('barcelona','url');
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action1: NuevoDestinoAction = new NuevoDestinoAction(destino);
        // actions
        const prevState2 = reducerDestinosViajes(prevState, action1);
        const action2: VoteUpAction = new VoteUpAction(destino);
        const prevState3: DestinosViajesState = reducerDestinosViajes(prevState2, action2);
        const action3: VoteResetAction = new VoteResetAction(destino);
        const newState: DestinosViajesState = reducerDestinosViajes(prevState3, action3);
        // asserts
        expect(newState.items[0].votes).toEqual(0);
    });
});

