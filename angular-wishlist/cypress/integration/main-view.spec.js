describe('ventana principal', () => {
    it('tiene encabezado correcto y en español por defecto', () => {
        cy.visit('http://localhost:4200');
        cy.contains('angular-wishlist');
        cy.get('h1 b').should('contain','HOLA es');
    });
});

describe('ventana reservas', () => {
    it('tiene link ir a detalle', () => {
        cy.visit('http://localhost:4200/reservas');
        cy.contains('Ir a detalle');
        cy.get('a').should('contain','Ir a login');
    });
});

describe('ventana login', () => {
    it('tiene los inputs campos vacios', () => {
        cy.visit('http://localhost:4200/login');
        cy.get('input[name="username"]').should('be.empty');
        cy.get('input[name="password"]').should('be.empty');
    });
});